//celine Tran 1938648
"use strict";

document.addEventListener("DOMContentLoaded", setup);
let globals = {}

function setup() {

    globals.term = document.querySelector('#cityInput');
    globals.section = document.querySelector('#output');
    globals.baseURL = "https://api.openweathermap.org/data/2.5/";
    globals.key = "57a94a13019a7bdbb869bd10552d0ba3";
    document.querySelector('form').addEventListener('submit', getInfo);

}

function getInfo(event) {
    event.preventDefault();

    const newLink = `${globals.baseURL}weather?q=${globals.term.value}&units=metric&APPID=${globals.key}`;
    console.log(newLink);
    fetchFromAPI(newLink, display);
}

function fetchFromAPI(newLink, successFunction) {
    fetch(newLink)
        .then(response => {
            if (response.ok)
                return response.json();
            throw new Error("Status code: " + response.status);
        })
        .then(obj => successFunction(obj))
        .catch(error => treatError(error));
}

function display(json) {
    let paragraph = document.createElement('p');
    globals.section.appendChild(paragraph);

    paragraph.textContent = `Weather:${json.weather[0].description}. Temp:${Math.round(json.main.temp)}°C`;
}

function treatError(error) {
    let paragraph = document.createElement('p');
    globals.section.appendChild(paragraph);
    paragraph.textContent = " Error:" + error + "Please try again in the paragraph in the section.";
}