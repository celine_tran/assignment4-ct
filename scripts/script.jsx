"use strict";
/**
 * This javaScript uses React and Fetches API from the openweather API to create a weather application which will display the weather inforomation of
 * a city whenever the user inputs a city name or a city name and the country name seperated by a comma.
 * @author Celine Tran 
 * @version 2020-12
 */

/**
 * @description This function renders the form by calling the CityForm class and applies it to the element with #form id in html.
 */
function setup() {
    ReactDOM.render(<CityForm />, document.querySelector('#form'));
}

/**
 * @description This class is called whenever ReactDom renders and will contain the majority of the code for the weather application.
 */
class CityForm extends React.Component {
    /**
     * @description This constructor initializes the state properties and other variable that will be needed further on the code like the api key.
     * It also calls the handle and submit function. 
     * @param {object} props The properties used for passing data form one component to another.
     */
    constructor(props) {
        super(props)
        this.state = {
            city: "",
            showError: false,
            showResult: false,
            errorMsg: "",
            //this is a way to initilize the img source that has no current image linked to it yet.
            srcURL: "data:,"
        }
        this.baseURL = "https://api.openweathermap.org/data/2.5/";
        this.key = "57a94a13019a7bdbb869bd10552d0ba3";
        this.iconLink = "http://openweathermap.org/img/w/";

        this.handleChange = this.change.bind(this);
        this.handleSubmit = this.submit.bind(this);
    }

    /**
     * @description This method updates the state of the input.
     * @param {event} e The change event
     */
    change(e) {
        this.setState({ city: e.target.value });
    }

    /** 
     * @description This method calls the getInfo method and surpresses any default behaviours.
     * @param {event} e The submit event
     */
    submit(e) {
        e.preventDefault()
        this.getInfo();
    }

    /**
     * @description This method uses Fetch and promise to retrieve json from the open wheather website and perform actions on it.
     * If the response if a 200 ok then it returns the json else it throws an error and catch will send it to errorRender.
     * @returns {Promise} The Promise object,containing the weather information converted to json
     */
    getInfo() {
        let link = `${this.baseURL}weather?q=${this.state.city}&units=metric&APPID=${this.key}`;
        let url = new URL(link);
        fetch(url)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                else if (response.status === 404) {
                    throw new Error(`City not found`);
                }
                else {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
            })
            .then(json => this.dataRender(json))
            .catch(error => this.errorRender(error));
    }

    /**
     * @description This will set the state of showError, showResult and errorMsg.
     * @param {error} error The error thrown from the getInfo
     */
    errorRender(error) {
        this.setState({ showError: true, showResult: false, errorMsg: error.message });

    }

    /**
     * @description This will set the state of 5 variables with the weather information from the api as well as the icon link.
     * @param {*} json The json passed form getInfo
     */
    dataRender(json) {
        if(json.error){
            showError(error);
        }
        else{
        this.setState({ showError: false, showResult: true });
        this.setState({
            description: json.weather[0].description, 
            temp: Math.round(json.main.temp), 
            icon: json.weather[0].icon,
            srcURL: this.iconLink + json.weather[0].icon + ".png", 
            humid: json.main.humidity, 
            wind: json.wind.speed
        });
    }
    }

    /**
     * @description This renders the components where the weather information will be put. 
     * It creates the elements for the data that was taken in dataRender or errorRender depending if the get info threw an error or not.
     */
    render() {
        const styleResult = {
            display: this.state.showResult ? "block" : "none"
        }
        const styleError = {
            display: this.state.showError ? "block" : "none"
        }

        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="city" id="cityLabel">City: </label>
                    <input type="text" id="cityInput" name="city" placeholder="Search..." onChange={this.handleChange} required />
                    <button type='submit' id="submit">Get Weather</button>
                </form>

                <div id="resultWeather" style={styleResult}>
                    <p id="des">Description: {this.state.description} </p>
                    <div id="info">
                        <p>Tempature: {this.state.temp}°C </p>
                        <p>Humidity: {this.state.humid}% </p>
                        <p>Wind Speed: {this.state.wind}m/s</p>
                    </div>
                    <img src={this.state.srcURL} alt="icon" />
                </div>

                <div id="error" style={styleError}>
                    <p >Error: {this.state.errorMsg} </p>
                </div>
            </div>
        );
    }
}

setup();